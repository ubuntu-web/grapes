const { app, BrowserWindow, ipcMain, dialog, shell, Notification } = require('electron')
const { exec } = require("child_process")
const path = require('path')
const { download } = require('electron-dl');

var mainWindow = undefined

function createWindow () {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false
    }
  })

  mainWindow.loadFile('src/index.html')
}

app.whenReady().then(() => {
  createWindow()

  app.on('activate', function () {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
  })
})

ipcMain.on('open-miniapp', (event, url) => {
  const appWindow = new BrowserWindow({
    width: 600,
    height: 400,
    webPreferences: {
      preload: path.join(__dirname, './src/js/app-preload.js'),
      nodeIntegration: false,
      enableRemoteModule: false,
      contextIsolation: true,
      sandbox: true,
      nodeIntegration: false
    }
  })

  // https://github.com/electron/electron/pull/573#issuecomment-520083828
  appWindow.webContents.session.webRequest.onHeadersReceived((details, callback) => {
    callback({ responseHeaders: Object.fromEntries(Object.entries(details.responseHeaders).filter(header => !/x-frame-options/i.test(header[0]))) });
  });

  appWindow.loadURL(url + '#ingrapes')

  event.sender.send('asynchronous-reply', 'opened window')
})

ipcMain.on('file', (event, file) => {
  dialog.showMessageBox({ buttons: ["Yes", "No"], message: "An app wants access to your computer. It wants to open the file: " + file }).then((response) => {
    if (response.response == 0) {
      shell.openPath(file)
    }
  })
})

ipcMain.on('wapp', (event, url) => {
  dialog.showMessageBox({ buttons: ["Yes", "No"], message: "An app wants access to your computer. It wants to install the web app from this URL: " + url }).then((response) => {
    if (response.response == 0) {
      url_filename = url.split('#').shift().split('?').shift().split('/').pop()
      new Notification({ title: 'Web Apps', body: 'Installing the web app.' }).show()
      download(mainWindow, url, { directory: '/tmp/grapes' , overwrite: true, filename: url_filename })
      exec(`pkexec wappinst ${path.join('/tmp/grapes', url_filename)}`, (error, stdout, stderr) => {
        if (error) {
          new Notification({ title: 'Web Apps', body: 'There was an error while installing the web app. Is the web app already installed? Also, it\' possible that you aren\'t connected to a network.' }).show()
        } else {
          new Notification({ title: 'Web Apps', body: 'Finished installing the web app.' }).show()
        }
      })
    }
  })
})

ipcMain.on('command', (event, command) => {
  dialog.showMessageBox({ buttons: ["Yes", "No"], message: "An app wants access to your computer. It wants to run: " + command }).then((response) => {
    if (response.response == 0) {
      exec(command)
    }
  })
})

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})