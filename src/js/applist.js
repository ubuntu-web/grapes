window.$ = window.jQuery = require('jquery');

function load_apps() {
    $.getJSON('http://localhost:4999/applist', function (applist) {
        console.log(applist)
        applist.forEach(function (app) {
            $('#applist').append(`
                <button onclick="open_app('${app.app_url}')" class="list-group-item d-flex justify-content-between align-items-center">
                    ${app.name}
                    <img src="${app.image_url}" height="32px">
                </button>
            `)
        })
    })
}

function open_app(url) {
    const { ipcRenderer } = require('electron')

    ipcRenderer.send('open-miniapp', url)
}

load_apps()