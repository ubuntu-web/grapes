const { ipcRenderer, shell } = require('electron');

process.once('loaded', () => {
  window.addEventListener('message', event => {
    switch(event.data.type) {
      case 'file':
        ipcRenderer.send('file', event.data.file);
        break;
      case 'wapp':
        ipcRenderer.send('wapp', event.data.url);
        break;
      case 'command':
        ipcRenderer.send('command', event.data.command);
        break;
    }
  });
});