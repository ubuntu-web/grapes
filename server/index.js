var applist = require("./applist.json");

const fs = require('fs')
const express = require('express')
const app = express()
const port = 4999

app.get('/applist', (req, res) => {
  fs.readFile('applist.json', 'utf8', function (err, data) {
    if (err) throw err;
    applist = JSON.parse(data);
    res.json(applist);
  });
})

app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`)
})